#// Course: CS 100 
#// Name: John Ericta
#// Login: jericta
#// Email: jeric001@ucr.edu
#// Assignment: Homework 7
#//
#// 
#//
#// I hereby certify that the code in this file represent my own
#// original individual work. Nowhere herein is there code from outside
#// resources such as another individual website, or publishings unless 
#// specifically designated as permissable by the instructor or TA
# =================================================================
#// my_find.sh 


#!/bin/sh

#check if theres a valid path argument
#sets default to current director
if [ $# -lt 1 ]
    then DIR=.
else
    DIR=$1
    shift
fi

#append path
name=$DIR/\*

#flags
f_flag=true
d_flag=true

#while number of arguments is greater than 0 run 
#name, type, print
#shifts to get the next argument at the current argument name, type, print
#shifts again for next iteration
while [ $# -gt 0 ]; do
  case $1 in
      -name)
	  shift
	  name=$DIR/\*$1\*
	  shift
	  ;;
      -type)
	  shift
	  type=$1
	  shift
	  ;;
      -print)
	  shift
	  name=$DIR/\*$1\*
	  shift
	  ;;
      *)
	  shift
	  ;;
  esac
done

#checks if f or d set flags to false
if [ -z $type ]
    then
    if [ "$type" == "f" ]
	then f_flag=false
    fi
    if [ "$type" == "d" ]
	then d_flag=false
    fi
fi

#for files in path check for files or directory
for f in $name
do
  if [ -f $f ] && [ $d_flag == "false" ]
      then continue
  fi
  if [ -d $f ] && [ $f_flag == "false" ]
      then continue
  fi
  if [ -f $f ] && [ $d_flag == "true" ]
      then echo $f
  fi
  if [ -d $f ] && [ $f_flag == "true" ]
      then echo $f
  fi
done
#// Course: CS 100 
#// Name: John Ericta
#// Login: jericta
#// Email: jeric001@ucr.edu
#// Assignment: Homework 6
#//
#//  Makefile
#//
#// I hereby certify that the code in this file represent my own
#// original individual work. Nowhere herein is there code from outside
#// resources such as another individual website, or publishings unless 
#// specifically designated as permissable by the instructor or TA
# =================================================================
#// Main Makefile

SRC = $(shell ls *.cc)
CXX = g++
CXX_FLAGS = -c -g

all: my_shell int exclude my_which dircheck my_find

tar: my_shell.cpp my_shellReadme.txt my_which.sh dircheck.sh my_find.sh int.cpp exclude.cpp Makefile myScript
	tar -cvzf hw7.tgz my_shell.cpp my_shellReadme.txt my_which.sh dircheck.sh my_find.sh int.cpp exclude.cpp Makefile myScript

int.o: int.cpp
	@echo making $@
	$(CXX) $(CXX_FLAGS) -o $@ int.cpp

exclude.o: exclude.cpp
	@echo making $@
	$(CXX) $(CXX_FLAGS) -o $@ exclude.cpp

my_shell.o: my_shell.cpp
	@echo making $@
	$(CXX) $(CXX_FLAGS) -o $@ my_shell.cpp

my_shell: my_shell.o
	@echo making $@
	$(CXX) -o $@ $^

int: int.o
	@echo making $@
	$(CXX) -o $@ $^

exclude: exclude.o
	@echo making $@
	$(CXX) -o $@ $^

my_which.sh:
	chmod u+x my_which.sh

dircheck.sh:
	chmod u+x dircheck.sh

my_find.sh:
	chmod u+x my_find.sh

clean:
	$(RM) *.o my_shell int exclude
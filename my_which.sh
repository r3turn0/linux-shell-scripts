#!/bin/sh

if [ -z "$1" ]
    then echo No command found
    exit
fi

#parse path
path=(`echo $PATH | tr ":" " "`)

#for each argument for which checks to see if the executable is found
#within the path 
for arg in $@
do
  for p in ${path[*]}
    do
  
    if [ -x $p/$arg ]
	then echo $p/$arg
	continue
    fi
  done
done      







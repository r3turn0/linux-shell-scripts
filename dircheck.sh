#// Course: CS 100 
#// Name: John Ericta
#// Login: jericta
#// Email: jeric001@ucr.edu
#// Assignment: Homework 7
#//
#// 
#//
#// I hereby certify that the code in this file represent my own
#// original individual work. Nowhere herein is there code from outside
#// resources such as another individual website, or publishings unless 
#// specifically designated as permissable by the instructor or TA
# =================================================================
#// dircheck.sh

#!/bin/sh
if [ -z "$1" ]
    then echo "Errorr invalid directory search"
    exit
fi

NUMFILES=0
NUMDIRS=0


for d in $@
do
  #iterates through the current director for the number of files or 
  #subdirectories
  find $d -type f -size 0 
  du $d -c
  for item in $d/*
  do
    if [ -f $item ]
	then 
	#echo found file $item
	NUMFILES=$[NUMFILES+1]
    fi
    if [ -d $item  ]
	then
	NUMDIRS=$[NUMDIRS+1]
	#echo found directories $item
    fi
  done
  echo "number of files in $d $NUMFILES" 
  echo "number of sub directors in $d $NUMDIRS"
  NUMFILES=0;
  NUMFIRS=0;
done
